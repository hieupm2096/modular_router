import 'package:flutter/material.dart';

/// {@template A2Page}
/// A2Page in module
/// {@endtemplate}
class A2Page extends StatelessWidget {
  /// A2Page
  const A2Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('A2 Page')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('This is A2 page'),
          ],
        ),
      ),
    );
  }
}
