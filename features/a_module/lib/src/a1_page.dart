import 'package:flutter/material.dart';

/// {@template A1Page}
/// A1Page in module
/// {@endtemplate}
class A1Page extends StatelessWidget {
  /// A1Page
  const A1Page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('A1 Page')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('This is A1 page'),
          ],
        ),
      ),
    );
  }
}
