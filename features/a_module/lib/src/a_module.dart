import 'package:a_module/src/a1_page.dart';
import 'package:a_module/src/a2_page.dart';
import 'package:a_module/src/a_page.dart';
import 'package:core/core.dart';

/// {@template a_module}
/// A module
/// {@endtemplate}
class AModule extends Module {
  /// {@macro a_module}
  @override
  List<Bind<Object>> get binds => [];

  @override
  List<ModularRoute> get routes => [
    ChildRoute<dynamic>('/', child: (context, args) => const APage()),
    ChildRoute<dynamic>('/a1', child: (context, args) => const A1Page()),
    ChildRoute<dynamic>('/a2', child: (context, args) => const A2Page()),
  ];
}
