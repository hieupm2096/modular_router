import 'package:core/core.dart';
import 'package:flutter/material.dart';

/// {@template HomePage}
/// HomePage in module
/// {@endtemplate}
class APage extends StatelessWidget {
  /// APage
  const APage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('A Page')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('This is A page'),
            const SizedBox(height: 20),
            TextButton(
              onPressed: () => Modular.to.pushNamed('./a1'),
              child: const Text('To A1 Page'),
            ),
            TextButton(
              onPressed: () => Modular.to.pushNamed('./a2'),
              child: const Text('To A2 Page'),
            ),
          ],
        ),
      ),
    );
  }
}
