import 'package:a_module/a_module.dart';
import 'package:core/core.dart';
import 'package:home/src/home_page.dart';

/// {@template home}
/// Home module
/// {@endtemplate}
class HomeModule extends Module {
  @override
  List<Bind<Object>> get binds => [];

  @override
  List<ModularRoute> get routes => [
        ChildRoute<dynamic>('/', child: (context, args) => const HomePage()),
        ModuleRoute<dynamic>('/a', module: AModule()),
      ];
}
