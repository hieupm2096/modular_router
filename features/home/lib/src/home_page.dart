import 'package:core/core.dart';
import 'package:flutter/material.dart';

/// {@template HomePage}
/// HomePage in module
/// {@endtemplate}
class HomePage extends StatelessWidget {
  /// {@macro HomePage}
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: const Text('Home Page')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('This is initial page'),
            const SizedBox(height: 20),
            TextButton(
              onPressed: () => Modular.to.pushNamed('./a/'),
              child: const Text('To A Page'),
            ),
          ],
        ),
      ),
    );
  }
}
