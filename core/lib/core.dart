library core;

export 'package:core/src/core_module.dart';
export 'package:core/src/repositories/repositories.dart';

// 3rd-party
export 'package:flutter_modular/flutter_modular.dart';
