import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// LocalDataSource interface
abstract class ILocalDataSource {
  /// store new [key] with [value]
  Future<void> create({required String key, required String value});

  /// retrieve value from [key]
  Future<String?> read({required String key});

  /// update [key] with new [value]
  Future<void> update({required String key, required String value});

  /// remove [key]
  Future<void> delete({required String key});
}

/// [ILocalDataSource] FlutterSecureStorage implementation
class SecureStorageLocalDataSource implements ILocalDataSource {
  /// create [SecureStorageLocalDataSource] instance with [FlutterSecureStorage]
  /// dependency
  SecureStorageLocalDataSource({required this.secureStorage});

  /// [FlutterSecureStorage] instance
  final FlutterSecureStorage secureStorage;

  @override
  Future<void> create({required String key, required String value}) async {
    await secureStorage.write(key: key, value: value);
  }

  @override
  Future<void> delete({required String key}) async {
    await secureStorage.delete(key: key);
  }

  @override
  Future<String?> read({required String key}) async {
    final value = await secureStorage.read(key: key);
    return value;
  }

  @override
  Future<void> update({required String key, required String value}) async {
    await secureStorage.write(key: key, value: value);
  }
}
