import 'package:core/core.dart';
import 'package:core/src/data_sources/local_data_source.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// [CoreModule] class
class CoreModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        Bind((i) => const FlutterSecureStorage()),
        Bind<ILocalDataSource>(
          (i) => SecureStorageLocalDataSource(secureStorage: i()),
        ),
      ];
}
