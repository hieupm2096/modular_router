import 'package:core/core.dart';
import 'package:dio/dio.dart';

/// [ApiClient]
class ApiClient {
  /// [ApiClient] constructor with [IAuthRepository] dependency
  ApiClient({required IAuthRepository authRepository})
      : _authRepository = authRepository;

  final IAuthRepository _authRepository;

  /// [Dio] instance
  Dio get dio {
    final dio = Dio();

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          final accessToken = await _authRepository.getAccessToken();
          if (accessToken != null) {
            options.headers['Authorization'] = 'Bearer $accessToken';
          }
        },
        onError: (err, handler) async {
          switch (err.type) {
            case DioErrorType.connectTimeout:
            case DioErrorType.sendTimeout:
            case DioErrorType.receiveTimeout:
              throw TimeoutException(err.requestOptions);
            case DioErrorType.response:
              switch (err.response?.statusCode) {
                case 401:
                case 403:
                  final refreshToken = await _authRepository.getRefreshToken();
                  if (refreshToken == null) {
                    throw UnauthorizedException(
                      requestOptions: err.requestOptions,
                      response: err.response,
                    );
                  }
                  final response = await dio.post<dynamic>(
                    '/user/refresh',
                    data: {'token': refreshToken},
                  );
                  // TODO: Check response

                  // TODO: parse response

                  // TODO: Save new access token and new refresh token

                  // TODO: Retry request
                  const token = '';
                  err.requestOptions.headers['Authorization'] = 'Bearer $token';
                  //create request with new access token
                  final opts = Options(
                    method: err.requestOptions.method,
                    headers: err.requestOptions.headers,
                  );
                  final cloneReq = await dio.request<dynamic>(
                    err.requestOptions.path,
                    options: opts,
                    data: err.requestOptions.data,
                    queryParameters: err.requestOptions.queryParameters,
                  );

                  return handler.resolve(cloneReq);

                case 400:
                case 404:
                case 409:
                case 500:
                  throw ResponseException(
                    requestOptions: err.requestOptions,
                    response: err.response,
                  );
                case 502:
                  throw ServiceUnavailableException(
                    requestOptions: err.requestOptions,
                  );
              }
              break;
            case DioErrorType.cancel:
              break;
            case DioErrorType.other:
              throw BadNetworkException(err.requestOptions);
          }

          return handler.next(err);
        },
      ),
    );
    return dio;
  }
}

/// {@template interceptor}
/// ResponseException
/// {@endtemplate}
class ResponseException extends DioError {
  /// {@macro ResponseException}
  ResponseException({
    required super.requestOptions,
    super.response,
  });

  @override
  String toString() {
    final data = response?.data;

    // TODO(hieupm): NoData format
    if (data != null) {
      try {
        // final res = NoData.fromJson(data);
        // final desc = res.mess?.description;
        // if (desc != null) return desc;
      } on Exception {
        return 'Có lỗi xảy ra';
      }
    }

    return 'Có lỗi xảy ra';
  }
}

/// {@template interceptor}
/// ServiceUnavailableException
/// {@endtemplate}
class ServiceUnavailableException extends DioError {
  /// {@macro ServiceUnavailableException}
  ServiceUnavailableException({required super.requestOptions});

  @override
  String toString() {
    return 'Dịch vụ hiện đang bị gián đoạn';
  }
}

/// {@template interceptor}
/// UnauthorizedException
/// {@endtemplate}
class UnauthorizedException extends DioError {
  /// {@macro UnauthorizedException}
  UnauthorizedException({
    required super.requestOptions,
    super.response,
  });

  @override
  String toString() {
    final data = response?.data;

    if (data != null) {
      // TODO(hieupm): return errorDesc from data
      // final mess = AuthMessage.fromJson(data);
      // final errorDesc = mess.errorDescription;
      // if (errorDesc != null) return errorDesc;
    }

    return 'Không có quyền truy cập';
  }
}

/// {@template interceptor}
/// BadNetworkException
/// {@endtemplate}
class BadNetworkException extends DioError {
  /// {@macro BadNetworkException}
  BadNetworkException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Không có kết nối, vui lòng thử lại';
  }
}

/// {@template interceptor}
/// TimeoutException
/// {@endtemplate}
class TimeoutException extends DioError {
  /// {@macro TimeoutException}
  TimeoutException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Kết nối bị gián đoạn, vui lòng thử lại';
  }
}
