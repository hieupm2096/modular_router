import 'package:core/src/data_sources/local_data_source.dart';

/// AuthRepository interface
abstract class IAuthRepository {
  /// retrieve refreshToken from local data source or memory
  Future<String?> getRefreshToken();

  /// retrieve refreshToken from local data source or memory
  Future<String?> getAccessToken();
}

/// AuthRepository implementation
class AuthRepository implements IAuthRepository {
  /// initialize [AuthRepository] with [ILocalDataSource] dependency
  AuthRepository({
    required ILocalDataSource localDataSource,
  }) : _localDataSource = localDataSource;

  final ILocalDataSource _localDataSource;

  String? _refreshToken;
  String? _accessToken;

  @override
  Future<String?> getRefreshToken() async {
    if (_refreshToken != null) {
      return _refreshToken;
    }
    final token = await _localDataSource.read(key: 'REFRESH_TOKEN');
    _refreshToken = token;
    return token;
  }

  @override
  Future<String?> getAccessToken() async {
    if (_accessToken != null) {
      return _accessToken;
    }
    final token = await _localDataSource.read(key: 'ACCESS_TOKEN');
    _accessToken = token;
    return token;
  }
}
