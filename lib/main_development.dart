// Copyright (c) 2022, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'package:flutter_modular/flutter_modular.dart';
import 'package:modular_router/app/app.dart';
import 'package:modular_router/app/view/app_module.dart';
import 'package:modular_router/bootstrap.dart';

void main() {
  bootstrap(() => ModularApp(module: AppModule(), child: const AppWidget()));
}
