import 'package:flutter_modular/flutter_modular.dart';
import 'package:home/home.dart';

class AppModule extends Module {
  @override
  List<Bind<Object>> get binds => [];

  @override
  List<ModularRoute> get routes => [
    ModuleRoute<dynamic>('/', module: HomeModule()),
  ];
}